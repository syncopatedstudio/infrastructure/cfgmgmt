# Configuration Management


```mermaid
---
title: Ansible Playbook Grapher
---
%%{ init: { "flowchart": { "curve": "bumpX" } } }%%
flowchart LR
	%% Start of the playbook 'base.yml'
	playbook_c7cd4183("base.yml")
		%% Start of the play 'Play: all (9)'
		play_e6dfacc2["Play: all (9)"]
		style play_e6dfacc2 fill:#b6c903,color:#ffffff
		playbook_c7cd4183 --> |"1"| play_e6dfacc2
		linkStyle 0 stroke:#b6c903,color:#b6c903
			pre_task_517e2c0d["[pre_task]  Symlink /etc/os-release to /etc/system-release"]
			style pre_task_517e2c0d stroke:#b6c903,fill:#ffffff
			play_e6dfacc2 --> |"1"| pre_task_517e2c0d
			linkStyle 1 stroke:#b6c903,color:#b6c903
			pre_task_20f3d274["[pre_task]  Include group variables"]
			style pre_task_20f3d274 stroke:#b6c903,fill:#ffffff
			play_e6dfacc2 --> |"2"| pre_task_20f3d274
			linkStyle 2 stroke:#b6c903,color:#b6c903
			pre_task_cea3fddd["[pre_task]  Include host variables"]
			style pre_task_cea3fddd stroke:#b6c903,fill:#ffffff
			play_e6dfacc2 --> |"3 [when: inventory_hostname != 'localhost']"| pre_task_cea3fddd
			linkStyle 3 stroke:#b6c903,color:#b6c903
			%% Start of the block ''
			block_f8eed450["[block] "]
			style block_f8eed450 fill:#b6c903,color:#ffffff,stroke:#b6c903
			play_e6dfacc2 --> |"4"| block_f8eed450
			linkStyle 4 stroke:#b6c903,color:#b6c903
			subgraph subgraph_block_f8eed450[" "]
				pre_task_56471b55[" Enable ssh daemon"]
				style pre_task_56471b55 stroke:#b6c903,fill:#ffffff
				block_f8eed450 --> |"1"| pre_task_56471b55
				linkStyle 5 stroke:#b6c903,color:#b6c903
				pre_task_07c7b515[" Print keyserver hostname"]
				style pre_task_07c7b515 stroke:#b6c903,fill:#ffffff
				block_f8eed450 --> |"2 [when: debugging is defined and keyserver is defined]"| pre_task_07c7b515
				linkStyle 6 stroke:#b6c903,color:#b6c903
				pre_task_f7643cb2[" Check if keys are present"]
				style pre_task_f7643cb2 stroke:#b6c903,fill:#ffffff
				block_f8eed450 --> |"3"| pre_task_f7643cb2
				linkStyle 7 stroke:#b6c903,color:#b6c903
				pre_task_9ed7df25[" Copy keys from remote host"]
				style pre_task_9ed7df25 stroke:#b6c903,fill:#ffffff
				block_f8eed450 --> |"4 [when: not keys.stat.exists and keyserver is defined]"| pre_task_9ed7df25
				linkStyle 8 stroke:#b6c903,color:#b6c903
			end
			%% End of the block ''
			%% Start of the block ''
			block_4a1a57a1["[block] "]
			style block_4a1a57a1 fill:#b6c903,color:#ffffff,stroke:#b6c903
			play_e6dfacc2 --> |"5"| block_4a1a57a1
			linkStyle 9 stroke:#b6c903,color:#b6c903
			subgraph subgraph_block_4a1a57a1[" "]
				pre_task_675ef4cc[" Check -march support"]
				style pre_task_675ef4cc stroke:#b6c903,fill:#ffffff
				block_4a1a57a1 --> |"1"| pre_task_675ef4cc
				linkStyle 10 stroke:#b6c903,color:#b6c903
				pre_task_72cec426[" Check output from grep command"]
				style pre_task_72cec426 stroke:#b6c903,fill:#ffffff
				block_4a1a57a1 --> |"2"| pre_task_72cec426
				linkStyle 11 stroke:#b6c903,color:#b6c903
				pre_task_d37eec7e[" Set architecture"]
				style pre_task_d37eec7e stroke:#b6c903,fill:#ffffff
				block_4a1a57a1 --> |"3 [when: supported_march.stdout_lines[0] == 'x86-64-v3']"| pre_task_d37eec7e
				linkStyle 12 stroke:#b6c903,color:#b6c903
				pre_task_941bce5c[" Set architecture"]
				style pre_task_941bce5c stroke:#b6c903,fill:#ffffff
				block_4a1a57a1 --> |"4 [when: supported_march.stdout_lines[0] == 'x86-64-v2']"| pre_task_941bce5c
				linkStyle 13 stroke:#b6c903,color:#b6c903
			end
			%% End of the block ''
			pre_task_3e3dd12c["[pre_task]  Include distro vars"]
			style pre_task_3e3dd12c stroke:#b6c903,fill:#ffffff
			play_e6dfacc2 --> |"6"| pre_task_3e3dd12c
			linkStyle 14 stroke:#b6c903,color:#b6c903
			pre_task_3c8178ff["[pre_task]  Set admin_group variable"]
			style pre_task_3c8178ff stroke:#b6c903,fill:#ffffff
			play_e6dfacc2 --> |"7"| pre_task_3c8178ff
			linkStyle 15 stroke:#b6c903,color:#b6c903
			%% Start of the role 'user'
			play_e6dfacc2 --> |"8"| role_ee11cbb1
			linkStyle 16 stroke:#b6c903,color:#b6c903
			role_ee11cbb1("[role] user")
			style role_ee11cbb1 fill:#b6c903,color:#ffffff,stroke:#b6c903
			%% End of the role 'user'
			%% Start of the role 'base'
			play_e6dfacc2 --> |"9"| role_593616de
			linkStyle 17 stroke:#b6c903,color:#b6c903
			role_593616de("[role] base")
			style role_593616de fill:#b6c903,color:#ffffff,stroke:#b6c903
			%% End of the role 'base'
			%% Start of the role 'network'
			play_e6dfacc2 --> |"10"| role_91e02cd2
			linkStyle 18 stroke:#b6c903,color:#b6c903
			role_91e02cd2("[role] network")
			style role_91e02cd2 fill:#b6c903,color:#ffffff,stroke:#b6c903
			%% End of the role 'network'
			%% Start of the role 'audio'
			play_e6dfacc2 --> |"11"| role_a5ca0b58
			linkStyle 19 stroke:#b6c903,color:#b6c903
			role_a5ca0b58("[role] audio")
			style role_a5ca0b58 fill:#b6c903,color:#ffffff,stroke:#b6c903
			%% End of the role 'audio'
			%% Start of the role 'ux'
			play_e6dfacc2 --> |"12"| role_13632c97
			linkStyle 20 stroke:#b6c903,color:#b6c903
			role_13632c97("[role] ux")
			style role_13632c97 fill:#b6c903,color:#ffffff,stroke:#b6c903
			%% End of the role 'ux'
			%% Start of the role 'applications'
			play_e6dfacc2 --> |"13"| role_b5fba9ff
			linkStyle 21 stroke:#b6c903,color:#b6c903
			role_b5fba9ff("[role] applications")
			style role_b5fba9ff fill:#b6c903,color:#ffffff,stroke:#b6c903
			%% End of the role 'applications'
			%% Start of the role 'virt'
			play_e6dfacc2 --> |"14"| role_a385e96c
			linkStyle 22 stroke:#b6c903,color:#b6c903
			role_a385e96c("[role] virt")
			style role_a385e96c fill:#b6c903,color:#ffffff,stroke:#b6c903
			%% End of the role 'virt'
			post_task_1b60f209["[post_task]  Cleanup old backup files"]
			style post_task_1b60f209 stroke:#b6c903,fill:#ffffff
			play_e6dfacc2 --> |"15 [when: cleanup is defined]"| post_task_1b60f209
			linkStyle 23 stroke:#b6c903,color:#b6c903
			post_task_55684f5a["[post_task]  Reboot host"]
			style post_task_55684f5a stroke:#b6c903,fill:#ffffff
			play_e6dfacc2 --> |"16 [when: reboot is defined]"| post_task_55684f5a
			linkStyle 24 stroke:#b6c903,color:#b6c903
			post_task_1425efe0["[post_task]  Wait for host to reboot"]
			style post_task_1425efe0 stroke:#b6c903,fill:#ffffff
			play_e6dfacc2 --> |"17 [when: reboot is defined]"| post_task_1425efe0
			linkStyle 25 stroke:#b6c903,color:#b6c903
		%% End of the play 'Play: all (9)'
	%% End of the playbook 'base.yml'
```


# Contributions
Contributions to this audio production workflow configuration project are welcome. If you encounter issues, have suggestions for improvements, or would like to add new features, please submit a pull request on our GitHub repository.

# License
This project is licensed under the MIT License. See the LICENSE file for more details.

# Acknowledgments

[Ansible Playbook Grapher][da74d179]

[da74d179]: https://github.com/haidaraM/ansible-playbook-grapher "Ansible Playbook Grapher"

## AUX Ansible role

Parts of the [mash-playbook](https://github.com/mother-of-all-self-hosting/mash-playbook) are used in this project.
