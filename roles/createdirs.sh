#!/bin/bash

ROLE_NAME="${1}"

mkdir -p $ROLE_NAME/{tasks,handlers,templates,files,vars,defaults,meta}

touch $ROLE_NAME/tasks/main.yml
touch $ROLE_NAME/handlers/main.yml
touch $ROLE_NAME/templates
touch $ROLE_NAME/files
touch $ROLE_NAME/vars/main.yml
touch $ROLE_NAME/defaults/main.yml
touch $ROLE_NAME/meta/main.yml

echo "Ansible role $ROLE_NAME has been created with the necessary folder structure."
