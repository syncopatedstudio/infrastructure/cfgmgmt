Users & Groups
--------------

This role creates unix accounts and groups

create home dir
copy ssh keys
copy files (i.e. .bash_profile) to user home
add users to proper groups
ensures sudo

users and the group(s) they belong to are set as variables in host_vars

to add a user to a host, add them to that var
to remove a user, remove them from that variable and run the remove-users task

groups:

opsadmin - basically the same as root, as users belonging to this group can run all commands with sudo



home directories are located on the ansible server within the normal unix /home directory. authorized users can log into the ansible server and put files they want to be distributed...

* note, binaries cannot be executed within the /home directory.....(set this in fstab eventually)


when you want to add a secondary gropu for a user, update the "secondary_groups" variable to include whatever groups you want. to remove a user from a group, just remove the group name from the secondary_group variable and run the playbook.....

secondary_groups variable is a comma seperate list of groups you wish to add the user to
e.g.
secondary_groups: "group1,group2,etc"



this role requires password_keys role as a dependency



when adding a group, set the 'unix_groups' var where you want the groups to be added. either for all hosts, in group_vars or specificy on invidual host in host_vars

if this group requires sudo privs, update the sudoers_group file in the users role



user passwords are randomly generated when creating the account. the user will be forced to change this password when logging in for the first time.

users will be emailed their ssh_keys as ssh access is only allowed with keys (no passwords)....



uids/gids

to ensure the user/group has the same uid/gid across all systems, set this variable in the approiate are (either unix_users or unix_groups)

group ids start at 2000
user ids start at 1040

we'll have to figure out a good way to keep track of uids/gids...but any tasks will fail if a duplicate id is detected....

###############
#ensure uid/gid is same on all hosts by grabbing the uid/gid from the above task and registering 
# it as a variable to use in the user cretion task below
# good explanation of using cut/awk/sed - http://unix.stackexchange.com/a/101426
- name: get the uid from newly created account
  local_action: shell: grep 'opsadmin' /etc/passwd | cut -d: -f3
  register: uid
################