---

- name: generate random password
  command: pwgen -s 8
  register: random_password
  run_once: True
  delegate_to: 127.0.0.1

- name: hash password
  expect:
    command: 'python -c ''from passlib.hash import sha512_crypt; print sha512_crypt.encrypt(raw_input("password: "))'''
    responses:
      (?i)password: "{{ random_password.stdout }}"
  register: expect_output
  run_once: True
  delegate_to: 127.0.0.1

- name: remove response prompt from stdout
  shell: 'echo ''{{ expect_output.stdout }}'' | sed -r "s/password: //g"'
  register: hashed_random_password
  run_once: True
  delegate_to: 127.0.0.1

- name: add user and generate ssh keys on cfgmgmtbot
  local_action: >
    user
    name={{ item.name }}
    uid={{ item.uid }}
    group={{ item.primary_group }}
    groups={{ item.secondary_groups }}
    append=yes
    shell={{ item.shell }}
    comment={{ item.email }}
    state=present
    password='{{ hashed_random_password.stdout }}'
    update_password=on_create
    generate_ssh_key=yes
    ssh_key_bits=2048
    ssh_key_file="{{ ssh_keys_path }}/{{ item.name }}/id_ed25519"
  run_once: True
  register: newuser_local

- name: force user to change password on first login to cfgmgmtbot
  local_action: command chage -d 0 {{ item.name }}
  run_once: True
  when: newuser_local.changed

- name: create authorized_keys on cfgmgmtbot
  local_action: >
    authorized_key
    user={{ item.name }}
    key="{{ lookup('file', ssh_keys_path + "/" + item.name + "/id_ed25519.pub") }}"
    exclusive=yes
  run_once: True

- name: add user to remote hosts
  user: >
    name={{ item.name }}
    uid={{ item.uid }}
    group={{ item.primary_group }}
    groups={{ item.secondary_groups }}
    append=yes
    shell={{ item.shell }}
    comment={{ item.email }}
    state=present
    password='{{ hashed_random_password.stdout }}'
    update_password=on_create
  register: newuser_remote

- name: force user to change password on first login
  command: chage -d 0 {{ item.name }}
  when: newuser_remote.changed

- name: create authorized_keys on remote hosts
  authorized_key: >
    user={{ item.name }}
    key="{{ lookup('file', ssh_keys_path + "/" + item.name + "/id_ed25519.pub") }}"
    exclusive=yes

- name: generate ssh config file and place in ssh keys dir
  template: >
    src=ssh_config.j2
    dest="{{ ssh_keys_path }}/{{ item.name }}/config"
    owner={{ item.name }}
    group={{ item.primary_group }}
    mode=0600
  run_once: True
  delegate_to: 127.0.0.1

- name: generate random password for ssh_keys zip archive
  command: pwgen -s 16
  register: zip_random_password
  when: newuser_local.changed
  run_once: True
  delegate_to: 127.0.0.1

- name: zip up ssh_keys and encrypt
  expect:
    chdir: "{{ ssh_keys_path }}"
    command: "zip -er {{ item.name }}_keys.zip {{ item.name }}"
    responses:
      (?i)password: "{{ zip_random_password.stdout }}"
  when: newuser_local.changed
  run_once: True
  delegate_to: 127.0.0.1

- name: email opsadmin with temp zip file pw
  local_action: >
    mail
    to={{ cfgmgmtadmin_email }}
    from="{{ cfgmgmtbot_mail }}"
    subject="heyooooo....new user info"
    body="hey, new user {{ item.name }} has been created and their ssh_keys zip pw is {{ zip_random_password.stdout }}"
  run_once: True
  when: newuser_local.changed

- name: create tmp email template to send to new user
  template: >
    src=new_user_email.j2
    dest={{ ssh_keys_path}}/temp_email.txt
    owner=opsadmin
    group=opsadmin
    mode=0644
  run_once: True
  delegate_to: 127.0.0.1

- name: email initial password to user
  local_action: >
    mail
    to={{ item.email }}
    from="{{ cfgmgmtbot_mail }}"
    subject="new user info"
    body="{{ lookup('file', ssh_keys_path + "/" + "temp_email.txt") }}"
    attach="{{ ssh_keys_path }}/{{ item.name }}_keys.zip"
  run_once: True
  when: newuser_local.changed

- name: remove tmp email template sent to user
  local_action: file path="{{ ssh_keys_path }}/temp_email.txt" state=absent
  run_once: True
