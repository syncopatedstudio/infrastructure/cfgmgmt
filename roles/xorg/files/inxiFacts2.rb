#!/usr/bin/env ruby

require 'json'

def parse_subkeys(line, data)
  keys = line.scan(/[\w\-]+:/)
  values = line.scan(/:\s+(.+)/).flatten

  keys.each_with_index do |key, index|
    data[key] = {} unless data[key]
    data[key][values[index]] = {}
  end
end

x = `sudo /usr/bin/inxi -v8`
data = {}
section = nil

x.each_line do |line|
  line.strip!

  if line =~ /^(.*?):$/
    section = $1
    data[section] = {}
  elsif section && line =~ /^\s+(.*?):\s+(.*)$/
    key = $1.strip
    value = $2.strip

    if data[section][key]
      parse_subkeys(line, data[section][key])
    else
      data[section][key] = value
    end
  end
end

json_data = data.to_json

File.open('/tmp/output.json', 'w') do |file|
  file.puts(json_data)
end
