#!/bin/bash
set -e

# set a trap to exit with CTRL+C
ctrl_c() {
        echo "** End."
        sleep 1
}

trap ctrl_c INT SIGINT SIGTERM ERR EXIT

# Check if the user is root
if [[ $(id -u) -eq 0 ]]; then
   echo "It is advised to not run this as root but as the user you wish to configure"
   exit 1
fi

# declare colors!
declare -rx ALL_OFF="\e[1;0m"
declare -rx BBOLD="\e[1;1m"
declare -rx BLUE="${BOLD}\e[1;34m"
declare -rx GREEN="${BOLD}\e[1;32m"
declare -rx RED="${BOLD}\e[1;31m"
declare -rx YELLOW="${BOLD}\e[1;33m"

# cli display functions
say () {
  local statement=$1
  local color=$2

  echo -e "${color}${statement}${ALL_OFF}"
}

wipe() {
tput -S <<!
clear
cup 1
!
}

wipe="false"

# and so it begins...
wipe && say "hello!\n" $GREEN && sleep 0.5

function prompt_for_userid() {
    read -p "Please enter the user id: " userid
    echo $userid
}

userid=$(prompt_for_userid)
echo "You entered: $userid"

# clean cache
sudo pacman -Scc --noconfirm > /dev/null
sudo pacman -Syu rsync openssh python-pip --noconfirm > /dev/null


say "-----------------------------------------------" $BLUE
say "enabling ssh" $BLUE
say "-----------------------------------------------\n" $BLUE

sudo systemctl enable sshd

sleep 0.5

if [[ $wipe == 'true' ]]; then wipe && sleep 1; fi
say "\n-----------------------------------------------" $BLUE

if [[ ! -f "/home/${userid}/.ssh/id_ed25519.pub" ]]; then
  read -p "Enter the username and hostname of the remote host (e.g. user@example.com): " REMOTE_HOST
  cd /home/$userid && rsync -avP --delete $REMOTE_HOST:~/.ssh .
  chown -R $userid:$userid /home/$userid/
else
  say "ssh keys present"
fi

if [[ $wipe == 'true' ]]; then wipe && sleep 1; fi
say "\n-----------------------------------------------" $BLUE
say "running ansible-pull" $BLUE
say "-----------------------------------------------\n" $BLUE

# ansible-pull -U git@github.com:SyncopatedLinux/cfgmgmt.git \
#              -C development \
#              -i inventory \
#              -e "newInstall=true"


if [ $? = 0 ]; then
  cd $HOME && yadm clone git@github.com:b08x/dots.git -f
fi
